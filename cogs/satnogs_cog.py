import discord
import requests
from discord.ext import tasks, commands
from dateutil import parser, tz
from datetime import datetime, timezone

try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin

SATNOGS_NETWORK_API_URL = 'https://network.satnogs.org/api/'
SATNOGS_VERIFY_SSL = True

class SatnogsCog(commands.Cog):
    def __init__(self, bot, token, station_id, channel_id):
        self.bot = bot
        self.token = token
        self.station_id = station_id
        self.channel_id = channel_id
        self.channel = {}
        self.jobs_list = []
        self.tz = tz.gettz('Poland')
        self.jobs.start()

    def cog_unload(self):
        self.jobs.cancel()

    def find_job_with_id(self, id):
        return next((x for x in self.jobs_list if int(x["id"]) == id), None)

    def remove_old_jobs(self):
        for job in self.jobs_list:
            end = parser.parse(job["end"])
            if end < datetime.now(timezone.utc):
                self.jobs_list.remove(job)

    def request_jobs(self):
        url = urljoin(SATNOGS_NETWORK_API_URL, 'jobs/')
        params = {
                'ground_station': self.station_id
        }
        headers = {'Authorization': 'Token {0}'.format(self.token)}

        response = requests.get(url, params=params, headers=headers, verify=SATNOGS_VERIFY_SSL, timeout=15)

        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError as http_error:
            print(http_error)

        return response.json() 

    async def notify_new_jobs(self, jobs):
        for job in jobs:
            id = int(job["id"])
            if self.find_job_with_id(id) is None:
                name = job["tle0"]
                start = parser.parse(job["start"]).astimezone(self.tz)
                end = parser.parse(job["end"]).astimezone(self.tz)
                freq = job["frequency"] / 1000000
                await self.channel.send(f'Zaplanowano obserwację satelity {name} od {start} do {end} na częstotliwości {freq:.3f} MHz: <https://network.satnogs.org/observations/{id}/>')
                self.jobs_list.append(job)

    @tasks.loop(seconds=60.0)
    async def jobs(self):
        print('Requesting jobs')
        jobs = self.request_jobs()
        print(f'response: {jobs}')

        await self.notify_new_jobs(jobs)

        self.remove_old_jobs()

        print(f'list: {self.jobs_list}')

    @jobs.before_loop
    async def before_printing(self):
        await self.bot.wait_until_ready()
        self.channel = self.bot.get_channel(self.channel_id)

