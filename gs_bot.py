import discord
import asyncio
import os
from dotenv import load_dotenv
from discord.ext import commands
from discord import app_commands
from cogs.satnogs_cog import SatnogsCog
from cogs.zmq_cog import ZmqCog

DISCORD_SERVER = int(os.getenv('DISCORD_SERVER_ID'))

class GSBot(commands.Bot):
    async def setup_hook(self):
        await self.tree.sync(guild = discord.Object(id = DISCORD_SERVER))

async def main():
    load_dotenv()
    DISCORD_TOKEN = os.getenv('DISCORD_TOKEN')
    SATNOGS_TOKEN = os.getenv('SATNOGS_API_TOKEN')
    SATNOGS_STATION = int(os.getenv('SATNOGS_STATION_ID'))
    DISCORD_CHANNEL = int(os.getenv('DISCORD_CHANNEL_ID'))
    ZMQ_ADDRESS = os.getenv('ZMQ_ADDRESS')
    ZMQ_PUB = os.getenv('ZMQ_PUB')
    ZMQ_SUB = os.getenv('ZMQ_SUB')

    print(DISCORD_TOKEN)
    print(SATNOGS_TOKEN)
    print(SATNOGS_STATION)
    print(DISCORD_CHANNEL)
    print(DISCORD_SERVER)
    print(ZMQ_ADDRESS)
    print(ZMQ_PUB)
    print(ZMQ_SUB)
    
    intents = discord.Intents.default()
    intents.message_content = True

    async with GSBot(command_prefix='$', intents=intents) as bot:
        await bot.add_cog(SatnogsCog(bot, SATNOGS_TOKEN, SATNOGS_STATION, DISCORD_CHANNEL))
        await bot.add_cog(ZmqCog(bot, ZMQ_ADDRESS, ZMQ_PUB, ZMQ_SUB, DISCORD_CHANNEL))
        await bot.start(DISCORD_TOKEN)


if __name__ == '__main__':
    asyncio.run(main())
