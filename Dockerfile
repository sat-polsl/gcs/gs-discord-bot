FROM python:3.10.8-slim-bullseye

WORKDIR app

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY cogs cogs
COPY gs_bot.py .

CMD ["python", "./gs_bot.py"]

