import discord
import zmq
import zmq.asyncio
import json
import os
from discord.ext import tasks, commands

DISCORD_SERVER = int(os.getenv('DISCORD_SERVER_ID'))
DISCORD_ROLE = int(os.getenv('DISCORD_ROLE_ID'))

class ZmqCog(commands.Cog):
    def __init__(self, bot: commands.Bot, address: str, pub: str, sub: str, channel_id: int):
        self._bot = bot
        self._channel_id = channel_id
        self._channel = {}
        self._ctx = zmq.asyncio.Context()

        self._sub_socket = self._ctx.socket(zmq.SUB)
        self._sub_socket.subscribe('relays_state')
        self._sub_socket.connect('tcp://' + address + ':' + sub)

        self._pub_socket = self._ctx.socket(zmq.PUB)
        self._pub_socket.connect('tcp://' + address + ':' + pub)

        self._subscriber.start()

    def cog_unload(self):
        self._subscriber.cancel()
    
    @discord.app_commands.command(name = 'gs_get_relays', description = 'Checks GS Relays state')
    @discord.app_commands.guilds(discord.Object(id = DISCORD_SERVER))
    @discord.app_commands.checks.has_role(DISCORD_ROLE)
    async def _gs_get_relays(self, interaction: discord.Interaction):
        self._pub_socket.send_string('get_relays', zmq.SNDMORE)
        self._pub_socket.send_string('{}')
        await interaction.response.send_message('Odczyt stanu przekaźników')

    @discord.app_commands.command(name = 'gs_set_relays', description = 'Sets given relay [rot, vhf, uhf] to given state [on,off]')
    @discord.app_commands.guilds(discord.Object(id = DISCORD_SERVER))
    @discord.app_commands.checks.has_role(DISCORD_ROLE)
    async def _gs_set_relays(self, interaction: discord.Interaction, relay: str, state: str):
        if relay == 'rot':
            relay = 'rotator'
        elif relay == 'vhf':
            relay = 'filter2m'
        elif relay == 'uhf':
            relay = 'filter70cm'
        payload = {relay: (True if state == 'on' else False)}
        print(payload)
        self._pub_socket.send_string('set_relays', zmq.SNDMORE)
        self._pub_socket.send_string(json.dumps(payload))
        await interaction.response.send_message(f'Ustawiono przekaźnik {relay} na {state}')

    @tasks.loop(seconds=0.1)
    async def _subscriber(self):
        try:
            msg = await self._sub_socket.recv_multipart()
        except zmq.ZMQError as e:
            print(e)

        print(msg)
        topic = msg[0].decode('utf-8')
        print(topic)
        if topic == 'relays_state':
            payload = json.loads(msg[1])
            print(payload)
            rot = payload['rotator']
            vhf = payload['filter2m']
            uhf = payload['filter70cm']
            await self._channel.send(f'Stan przekaźników: Rotor: {rot}, VHF: {vhf}, UHF: {uhf}')

    @_subscriber.before_loop
    async def before_printing(self):
        await self._bot.wait_until_ready()
        self._channel = self._bot.get_channel(self._channel_id)
